// Mundo.h: interface for the CMundoServidor class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
//#include "Raqueta.h"
#include "DatosMemCompartida.h"

//Includes para el cliente-servidor

#include <pthread.h>

//Includes de las tuberias

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#include <sys/mman.h>

//include socket
#include "Socket.h"



class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	//Funcion comandos para las teclas cliente-servidor
	void RecibeComandosJugador();
	//COMUNICACION LOGGER
	FILE *mipipe;
	char buffer[128];
	Esfera esfera;
	std::vector<Plano> paredes;
	std::vector<Esfera> esferas;
	std::vector<Esfera> disparos1;
	std::vector<Esfera> disparos2;	
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	int botes;
	//comunicacion con el bot
	/*char *org;
	DatosMemCompartida datosm;
	DatosMemCompartida *datosmpunt;*/
	unsigned t0, t1;
	double time;

	//Tuberia cliente-servidor  no necesaria p5
	//int tuberiacs;

	//Tuberia teclas cliente servidor  no necesaria p5
	//int tuberiateclas;

	//Thread cliente-servidor
	pthread_t thid1;

	//Socket p5
	Socket socket_conexion, socket_comunicacion;


};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
