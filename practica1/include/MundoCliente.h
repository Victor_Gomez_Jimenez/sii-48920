// MundoCliente.h: interface for the CMundoCliente class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#include "Esfera.h"
//#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include <sys/mman.h>

#include <pthread.h>

//include socket 
#include "Socket.h"

class CMundo 
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	//COMUNICACION CON LOGGER	
	//FILE *mipipe;
	//char buffer[128];
	Esfera esfera;
	std::vector<Plano> paredes;
	std::vector<Esfera> esferas;
	std::vector<Esfera> disparos1;
	std::vector<Esfera> disparos2;	
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	int botes;
	//COMUNICACION CON BOT
	char *org;
	DatosMemCompartida datosm;
	DatosMemCompartida *datosmpunt;
	unsigned t0, t1;
	double time;
	//Tuberia cliente-servidor  ya no necesaria practica 5
	//int tuberiacs;
	//int tuberiateclas;

	//Socket p5
	Socket socket_comunicacion;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
