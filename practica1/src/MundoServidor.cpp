// Autor:Víctor Gómez
// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////



#include <ctime> 
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <signal.h> 
#include <fcntl.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h> 
#include <iostream>
#include <pthread.h>

using namespace std;

void* hilo_comandos(void* d)
{
	CMundo* p=(CMundo*) d;
	p->RecibeComandosJugador();
}

void CMundo::RecibeComandosJugador()
{
	while(1)
	{
		usleep(10);
		char cadena[100];
		//read(tuberiateclas,cadena,sizeof(cadena)); lo quitamos para la p5
		socket_comunicacion.Receive(cadena,sizeof(cadena));  //socket para la comunicacion del cliente con el servirdor, aqui se prepara para recivir las teclas pulsadas
		unsigned char key;
		sscanf(cadena,"%c",&key);
		if(key=='s')jugador1.velocidad.y=-4;
		if(key=='w')jugador1.velocidad.y=4;
		if(key=='l')jugador2.velocidad.y=-4;
		if(key=='o')jugador2.velocidad.y=4;
		if(key=='m'){
		if(disparos2.size()==0){
		Esfera e;
		e.radio=0.2;
		e.centro.x=jugador2.x1-0.6;
		e.centro.y=jugador2.y1+(jugador2.y2-jugador2.y1)/2;
		e.velocidad.y=0;
		e.velocidad.x=-3;

		e.rojo=0;

		e.verde=255;

		e.azul=0;
		disparos2.push_back(e);
		}}
		if(key=='c'){
		if(disparos1.size()==0){
		Esfera e;
		e.radio=0.2;
		e.centro.x=jugador1.x1+0.6;
		e.centro.y=jugador1.y1+(jugador1.y2-jugador1.y1)/2;
		e.velocidad.y=0;
		e.velocidad.x=3;
		e.rojo=255;
		e.verde=0;
		e.azul=0;
		disparos1.push_back(e);
		}}
}


	
}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
//comunicacion con logger
sprintf(buffer, "FIN DEL JUEGO \n");
fputs(buffer,mipipe);
fclose(mipipe);
//comunicacion con bot
//munmap(org, sizeof(datosm));

//Tuberia Cliente-Servidor // no necesaria en la P5
	//close(tuberiacs);

	//Tuberia teclas cliente-servidor
	//close(tuberiateclas);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

// lo mismo para esferas y los distintos disparos
	for(i=0;i<esferas.size();i++)
		esferas[i].Dibuja();
	for(i=0;i<disparos1.size();i++)
		disparos1[i].Dibuja();
	for(i=0;i<disparos2.size();i++)
		disparos2[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	
	t1 = clock();
	time = (double(t1-t0)*10/CLOCKS_PER_SEC);
	//cout << "Execution Time: " << time << endl;
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	
	int i;
	int j;
//recorremos cada vector para ejecutar el metodo mueve de cada una de sus esferas
	
for(i=0;i<esferas.size();i++)
		esferas[i].Mueve(0.025f);
	for(i=0;i<disparos1.size();i++)
		disparos1[i].Mueve(0.025f);
	for(i=0;i<disparos2.size();i++)
		disparos2[i].Mueve(0.025f);
	
	for(i=0;i<paredes.size();i++)
	{
//aqui se gestiona el rebote de las raquetas con las paredes
		
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);

//ahora lo mismo pero para esferas
		for(j=0;j<esferas.size();j++)
		{
		paredes[i].Rebota(esferas[j]);
		}
		
	}
//introducimos un contador de rebotes (de la primera esfera) contra las raquetas

	if((jugador1.Rebota(esferas[0]))||(jugador2.Rebota(esferas[0]))){
			botes++;
			}
//si hay una sola esfera y los rebotes son mayores que quince, agregamos una segunda esfera

//quitamos la segunda esfera para acomodarnos a la nueva practica 
		/*if((esferas.size()==1)&&(botes>15)){
		Esfera e;
		e.radio=1;
		e.centro.x=0;
		e.centro.y=rand()/(float)RAND_MAX;
		e.velocidad.x=2+2*rand()/(float)RAND_MAX;
		e.velocidad.y=2+2*rand()/(float)RAND_MAX;
		esferas.push_back(e);
		botes=0;
		}*/

		
//rebote esferas con las raquetas
	for(int j=0;j<esferas.size();j++)
		{
		jugador1.Rebota(esferas[j]);
		jugador2.Rebota(esferas[j]);
		}
//impacto de los disparos del jugador1 contra la raqueta del jugador 2
	for(int j=0;j<disparos1.size();j++)
		{
		if(jugador2.Rebota(disparos1[j]))
		{
			if((jugador2.y2-jugador2.y1)>1.5){
			jugador2.y2=jugador2.y2-0.5;
			jugador2.y1=jugador2.y1+0.5;}
			disparos1.erase(disparos1.begin(),disparos1.begin()+disparos1.size());	
		}
		}
//impacto de los disparos del jugador2 contra la raqueta del jugador 1
	for(int j=0;j<disparos2.size();j++)
		{
		if(jugador1.Rebota(disparos2[j]))
		{
			if((jugador1.y2-jugador1.y1)>1.5){
			jugador1.y2=jugador1.y2-0.5;
			jugador1.y1=jugador1.y1+0.5;}
			disparos2.erase(disparos2.begin(),disparos2.begin()+disparos2.size());	
		}
		}
//gestion del impacto de las esferas conrta el fondo izquierdo		
	
	for(int j=0;j<esferas.size();j++){
		
	if(fondo_izq.Rebota(esferas[j]))
	{
		esferas.erase(esferas.begin(),esferas.begin()+esferas.size());
		disparos1.erase(disparos1.begin(),disparos1.begin()+disparos1.size());	
		disparos2.erase(disparos2.begin(),disparos2.begin()+disparos2.size());	
		Esfera e;
		e.radio=0.5;
		e.centro.x=0;
		e.centro.y=rand()/(float)RAND_MAX;
		e.velocidad.x=2+2*rand()/(float)RAND_MAX;
		e.velocidad.y=2+2*rand()/(float)RAND_MAX;
		esferas.push_back(e);
		botes=0;
		puntos2++;
		//comunicacion
		sprintf(buffer, "MARCA JUGADOR 2 LLEVA %d puntos\n",puntos2);
		fputs(buffer,mipipe);
		
//los siguientes if son para que al impactar la esfera contra el fondo izquierdo, las raquetas
//vuelvas a su estado natural,en caso de haber sido reducidas
		if((jugador1.y2-jugador1.y1)<1.5) 
		{
			if((jugador1.y2<4)&&(jugador1.y1>-4))
				{
				jugador1.y2=jugador1.y2+0.5;	
				jugador1.y1=jugador1.y1-0.5;
				}
			if(jugador1.y2>4)
				{
				jugador1.y1=jugador1.y1-1.0;
				}
			if(jugador1.y1<-4)
				{
				jugador1.y2=jugador1.y2+1.0;
				}
		}
//aqui para el jugador2
		if((jugador2.y2-jugador2.y1)<1.5) 
		{
			if((jugador2.y2<4)&&(jugador2.y1>-4))
				{
				jugador2.y2=jugador2.y2+0.5;	
				jugador2.y1=jugador2.y1-0.5;
				}
			if(jugador2.y2>4)
				{
				jugador2.y1=jugador2.y1-1.0;
				}
			if(jugador2.y1<-4)
				{
				jugador2.y2=jugador2.y2+1.0;
				}
		}
		
	}
	}
	
//los siguientes if son para que al impactar la esfera contra el fondo derecho, las raquetas
//vuelvas a su estado natural,en caso de haber sido reducidas
	for(int j=0;j<esferas.size();j++){
	if(fondo_dcho.Rebota(esferas[j]))
	{
		esferas.erase(esferas.begin(),esferas.begin()+esferas.size());
		disparos1.erase(disparos1.begin(),disparos1.begin()+disparos1.size());	
		disparos2.erase(disparos2.begin(),disparos2.begin()+disparos2.size());	
		Esfera e;
		e.radio=0.5;
		e.centro.x=0;
		e.centro.y=rand()/(float)RAND_MAX;
		e.velocidad.x=2+2*rand()/(float)RAND_MAX;
		e.velocidad.y=2+2*rand()/(float)RAND_MAX;
		esferas.push_back(e);
		botes=0;
		
		puntos1++;
		//comunicacion
		
		sprintf(buffer, "MARCA JUGADOR 1 LLEVA %d puntos\n",puntos1);
		fputs(buffer,mipipe);
	
		if((jugador1.y2-jugador1.y1)<1.5) 
		{
			if((jugador1.y2<4)&&(jugador1.y1>-4))
				{
				jugador1.y2=jugador1.y2+0.5;	
				jugador1.y1=jugador1.y1-0.5;
				}
			if(jugador1.y2>4)
				{
				jugador1.y1=jugador1.y1-1.0;
				}
			if(jugador1.y1<-4)
				{
				jugador1.y2=jugador1.y2+1.0;
				}
		}
//aqui para el jugador2
		if((jugador2.y2-jugador2.y1)<1.5) 
		{
			if((jugador2.y2<4)&&(jugador2.y1>-4))
				{
				jugador2.y2=jugador2.y2+0.5;	
				jugador2.y1=jugador2.y1-0.5;
				}
			if(jugador2.y2>4)
				{
				jugador2.y1=jugador2.y1-1.0;
				}
			if(jugador2.y1<-4)
				{
				jugador2.y2=jugador2.y2+1.0;
				}
		}
	}
	}
//los disparos del jugador1 al chocar con el fondo, desaparecen
	for(int j=0;j<disparos1.size();j++){
		if(fondo_dcho.Rebota(disparos1[j])){
			disparos1.erase(disparos1.begin()+j);

			}
	}
//los disparos del jugador2 al chocar con el fondo, desaparecen
	for(int j=0;j<disparos2.size();j++){
		if(fondo_izq.Rebota(disparos2[j])){
			disparos2.erase(disparos2.begin()+j);

			}
	}

	if((puntos1==3)||(puntos2==3))
		exit(0);
// comunicacion con el bot	
/*
	switch(datosmpunt->accion){
		case 1: OnKeyboardDown('w',0,0); break;
		case -1: OnKeyboardDown('s',0,0); break;
		case 0: break;
	}
	if (time>10){
	switch(datosmpunt->accion2){
		case 1: jugador2.velocidad.y=4; break;
		case -1: jugador2.velocidad.y=-4; break;
		case 0: break;
	}
	}	
	datosmpunt->esfera=esferas[0];//devuelve posicion de la primera esfera
	datosmpunt->raqueta1=jugador1; //devuelve posición de raqueta1
	datosmpunt->raqueta2=jugador2; //devuelve posición de raqueta2*/

//Comunicacion Cliente-Servidor

	char cadcs[200];

sprintf(cadcs,"%f %f %f %f %f %f %f %f %f %f %d %d", esferas[0].centro.x,esferas[0].centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);

// version sockets ********************  en esta parte envia las posiciones necesarias al cliente
socket_comunicacion.Send(cadcs,sizeof(cadcs));





	//write(tuberiacs,cadcs,sizeof(cadcs)); //ya no hace falta en p5

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
		
 	//RecibeComandosJugador();
		

	switch(key)
	{
/*
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	//case 's':jugador1.velocidad.y=-4;break;
	//case 'w':jugador1.velocidad.y=4;break;
	/*case 'l':jugador2.velocidad.y=-4;
			t0 = clock();break;
	case 'o':jugador2.velocidad.y=4;
			t0 = clock();break;
	
*/
//las teclas que daran lugar a los disparos de los jugadores	
	
//quitamos los disparospara adaptarnos al guion 
/*
	case 'c':
		if(disparos1.size()==0){
		Esfera e;
		e.radio=0.2;
		e.centro.x=jugador1.x1+0.6;
		e.centro.y=jugador1.y1+(jugador1.y2-jugador1.y1)/2;
		e.velocidad.y=0;
		e.velocidad.x=3;
		e.rojo=255;
		e.verde=0;
		e.azul=0;
		disparos1.push_back(e);
		}
		break;

	case 'm':
		if(disparos2.size()==0){
		Esfera e;
		e.radio=0.2;
		e.centro.x=jugador2.x1-0.6;
		e.centro.y=jugador2.y1+(jugador2.y2-jugador2.y1)/2;
		e.velocidad.y=0;
		e.velocidad.x=-3;
		e.rojo=0;
		e.verde=255;
		e.azul=0;
		disparos2.push_back(e);
		}
		break;
		*/
	}
}

void CMundo::Init()
{
	t0=clock();
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	//crear la primera esfera
		Esfera e;
		e.radio=0.5;
		e.centro.x=0;
		e.centro.y=rand()/(float)RAND_MAX;
		e.velocidad.x=2+2*rand()/(float)RAND_MAX;
		e.velocidad.y=2+2*rand()/(float)RAND_MAX;
		esferas.push_back(e);
	botes=0;

// comunicacion logger



mipipe=fopen("pipetest","w");

//bot
/*
int f=open("/tmp/datosComp.txt",O_RDWR|O_CREAT|O_TRUNC,0666);
	if(f<0)
		perror("error al abrir el fichero");
	write (f,&datosm,sizeof(datosm));
//Convierte a char 
	org=(char*)mmap(NULL,sizeof(datosm),PROT_WRITE|PROT_READ,MAP_SHARED,f,0);
//Comprobar siempre que de error
	close (f);
	datosmpunt=(DatosMemCompartida*)org;
	datosmpunt->accion=0;
	datosmpunt->accion2=0;
*/

//CLIENTE-SERVIDOR

	//Abrimos la tubería de comunicacion entre cliente y servidor://ahora seran sockets en p5
	//tuberiacs=open("/tmp/csfifo", O_WRONLY);	

	//Thread cliente-servidor
	pthread_create(&thid1, NULL, hilo_comandos,this);


//version con sockets
	socket_conexion.InitServer((char*)"127.0.0.1",3100);       //se inicia el socket de conexion,  con la ip y el puerto indicados
	socket_comunicacion=socket_conexion.Accept();  // con Accept se devuelve un socket con la direccion del cliente
	char nombre_cliente[100];
	socket_comunicacion.Receive(nombre_cliente,sizeof(nombre_cliente)); 
	printf("Conectado el cliente, %s\n",nombre_cliente);

	//Tuberia teclas
	//tuberiateclas=open("/tmp/teclasfifo", O_RDONLY);

}
