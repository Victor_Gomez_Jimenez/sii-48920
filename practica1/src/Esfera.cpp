// Autor:Víctor Gómez
// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
	azul=0;
	rojo=255;
	verde=255;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(rojo,verde,azul);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
centro=centro+velocidad*t;
radio=radio-0.03*t;
if (radio<0.3) {
	radio=0.3;
		}
}

